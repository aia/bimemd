"""
*******  Two-dimensional Multivariate Empirical Mode Decomposition (MEMD) introduced in [1]  **********
The algorithm is based on the single-dimension MEMD of [2] and the implementation of its two-dimensional 
extension by [3].


@author: Esther Maeteling; e.maeteling@aia.rwth-aachen.de

Copyright © 2020-2022 Esther Maeteling 
This code is published under the GNU General Public License. Please check the "License" document 
provided in the repository for further information.

Portion of this code (matlab implementation) Copyright © Oct-2009 Naveed ur Rehman and Danilo P. Mandic
in the framework of the single-dimension MEMD [2]


# =======================================================================================================
# explanation of input variables:
# =======================================================================================================
    
imf = biMEMD.getbiMEMD(data,ndir,nIMF,stopCrit,*arg)

(i) imf = biMEMD.getbiMEMD(data,ndir,nIMF,'classic',sd,sd2,tol)
(ii) imf = biMEMD.getbiMEMD(data,ndir,nIMF,'fix',siftings)

data: multivariate, two-dimensional data - first two dimensions represent each variate, third dimension  
      are the different channels
ndir: number of projection directions
nIMF: total number of IMFs including the residual (if data cannot be decomposed in nIMF- modes, the 
      last mode(s) are just zero)
stopCrit: 'classic' for traditional stopping criterion with sd, sd2, tol
          'fix' for fixed number of siftings
sd,sd2,tol: stopping criteria for 'classic' (see [2] for detailed information)
      default values to use:
      sd = 0.01
      sd2 = 0.1
      tol = 0.01
siftings: stopping criteria for 'fix': for each IMF, specify the number of sifts   
  
imf = extracted IMFs


# =======================================================================================================
[1] Maeteling, E. & Schroeder, W. (2022) Analysis of spatiotemporal inner-outer large-scale
    interactions in turbulent channel flow by multivariate empirical mode decomposition. Physical 
    Review Fluids 7, 034603. 
    
[2] Rehman, N., & Mandic, D. P. (2010). Multivariate empirical mode decomposition. Proceedings of the 
    Royal Society A: Mathematical, Physical and Engineering Sciences, 466(2117), 1291-1302.
    
[3] Xia, Y., Zhang, B., Pei, W., & Mandic, D. P. (2019). Bidimensional Multivariate Empirical Mode 
    Decomposition With Applications in Multi-Scale Image Fusion. IEEE Access, 7, 114261-114270.
# =======================================================================================================
"""

import time
import sys
from scipy import ndimage
import numpy as np
from scipy.sparse import csr_matrix, linalg, vstack
from sksparse.cholmod import cholesky_AAt

# =======================================================================================================
# main part
# =======================================================================================================
def getbiMEMD(data_input,ndir,total_nIMF,stopCrit,*arg):
    
    # check input arguments
    if len(arg) != 1 and len(arg) != 3:
        sys.exit("specify one argument for fixed siftings or three arguments for traditional stopping criterion")
    elif len(arg) == 1 and stopCrit != 'fix':
        sys.exit("specify one argument for fixed siftings or three arguments for traditional stopping criterion")
    elif len(arg) == 3 and stopCrit != 'classic':
        sys.exit("specify one argument for fixed siftings or three arguments for traditional stopping criterion")

    if len(arg) == 3:
        sd,sd2,tol = arg[0],arg[1],arg[2]
    elif len(arg) == 1:
        siftings = np.array(np.squeeze(arg), dtype = int)
        
    # --- init --- #
    global M, N, N_dim, dir_vec
    
    # dimensions
    N_dim = data_input.shape[2] # number of variates
    M = data_input.shape[0] # size of 2D data
    N = data_input.shape[1] # size of 2D data
    
    # get low-discrepancy sequences and convert to projection directions
    seq = hamm(ndir)
    dir_vec = get_dir(seq, ndir)
    
    # init for while loop
    n_imf = 1 # current imf
    nbit = 0 # counts siftings
    maxIterations = 100 # max sifting operations
    data = data_input
    q = [''] * total_nIMF
    
    # set timer
    start_time = time.time() # measure time for each IMF calculation
    
    # main loop for determining all IMF
    while stop_emd(data,ndir) != True and (n_imf < total_nIMF):
        
        curr_imf = data
        
        if stopCrit == 'classic':
            [stop_sift, env_mean] = stop_sifting(curr_imf,ndir,sd,sd2,tol)
        elif stopCrit == 'fix':
            [stop_sift, env_mean] = stop_sifting(curr_imf,ndir,siftings[n_imf-1],nbit)
        print('1. sifting done', flush=True)
        
        # In case the current mode is so small that machine precision can cause
        # spurious extrema to appear
        if (np.max(np.absolute(curr_imf))) < (1e-10)*(np.max(np.absolute(data_input))):
            print('forced stop of EMD: too small data values', flush=True)
            break
    
        # sifting process for one IMF
        while stop_sift != True and (nbit < maxIterations):
            curr_imf = curr_imf - env_mean
            if stopCrit == 'classic':
                [stop_sift, env_mean] = stop_sifting(curr_imf,ndir,sd,sd2,tol)
            elif stopCrit == 'fix':
                [stop_sift, env_mean] = stop_sifting(curr_imf,ndir,siftings[n_imf-1],nbit)
            print(str(nbit+2) + '. sifting done', flush=True)
            nbit += 1
            
            if nbit >= maxIterations:
                print("forced stop of sifting: too many iterations", flush=True)
        
        # store IMF and determine new input for next sifting
        q[n_imf-1] = curr_imf
        n_imf += 1
        nbit = 0
        
        elapsed_time = time.time() - start_time
        print("elapsed time for " + str(n_imf - 1) + ". IMF: " + str(elapsed_time) + " seconds", flush=True)
        data = data - curr_imf
    
    # store residuum
    q[n_imf-1] = data    
    return q
    

# =======================================================================================================
# stop EMD (if less than 3 extrema in one projection)
#
# input:
# data: current data/residual
# ndir: number of projections
#
# output:
# stp: True --> stop criterion is reached and sifing stops, otherwise False 
#
# details:
# current residual is projected step-wise in all directions; for each direction, 
# the locations of local extrema are determined by regional max value (windowing)
# with kernel size 3 and their number is calculated; if one projection direction
# has less than three extrema, the stopping criterion is reached
# =======================================================================================================


def stop_emd(data,ndir):
    global dir_vec
    ner = np.zeros([ndir,2])
    
    # loop for each projection direction
    for it in range(ndir):
        # Projection of input signal on n-th (out of total ndir) direction vectors
        y = data * dir_vec[it]
        
        # sum over all projected variates yielding a 2D field
        y = np.sum(y, axis = 2)
        
        # Calculation of extrema positions of the projected signal
        lmax = ndimage.maximum_filter(y,size=3) # local max values
        max_logic = (y == lmax) # convert local max values to binary mask
        lmin = ndimage.minimum_filter(y,size=3)
        min_logic = (y == lmin)
        
        # total number of extrema
        ner[it,:] = [np.sum(max_logic), np.sum(min_logic)]
    
    # Stops if one of the projected signals has less than 3 extrema
    stp = np.any(ner < 3)
    return stp # true means that stop criterion is reached and sifing stops


# =======================================================================================================
# get projection vectors
#
# input:
# seq: low-discrepancy sequences determined by "hamm"
# ndir: number of projections
#
# output:
# dir_vec: list with direction vectors for each of the ndir diretions
# =======================================================================================================


def get_dir(seq, ndir):
    global M, N, N_dim
    dir_vec = [''] * ndir # size of each entry is M x N x N_dim
    dir_t = np.zeros([1,N_dim])
    
    for it in range(ndir):
        
        # multivariate signal
        if (N_dim > 3): 
            # Linear normalization of Hammersley sequence in the range of -1.00 - 1.00
            b_temp = 2 * seq[:,it] - 1
        
            # Find angles corresponding to the normalized sequence
            tht = np.arctan2(np.sqrt(np.flipud(np.cumsum(np.flipud(b_temp[1:N_dim]**2)))),b_temp[0:N_dim-1])
            # Find coordinates of unit direction vectors on n-sphere
            dir_t[0,0] = 1
            dir_t[0,1:N_dim] = np.cumprod(np.sin(tht))
            dir_t[0,0:N_dim-1] = np.cos(tht) * dir_t[0,0:N_dim-1]
        
            temp = np.zeros([M,N,N_dim])
            for i in range(N_dim): 
                temp[:,:,i] = dir_t[0,i]
            dir_vec[it] = temp
    
        
        # trivariate signal
        elif (N_dim == 3):
            # Linear normalization of Hammersley sequence in the range of -1.0 - 1.0
            # everything greater than 1 or smaller than -1 is set to 1/-1
            tt = 2 * seq[0,it] - 1
            if tt>1: tt = 1
            if tt<-1: tt = -1
            
            # Normalize angle from 0 - 2*pi
            phirad = seq[1,it] * 2 * np.pi
            st = np.sqrt(1.0-tt*tt)
            
            temp = np.zeros([M,N,3])
            temp[:,:,0] = st * np.cos(phirad)
            temp[:,:,1] = st * np.sin(phirad)
            temp[:,:,2] = tt
            dir_vec[it] = temp    
    
        #Bivariate signal
        else:  
            temp = np.zeros([M,N,2])
            temp[:,:,0] = np.cos(2*np.pi*(it+1)/ndir)
            temp[:,:,1] = np.sin(2*np.pi*(it+1)/ndir)
            dir_vec[it] = temp
    
    return dir_vec


# =======================================================================================================
# envelope_mean: computes local mean of envelopes and mode amplitude estimate
#
# input:
# curr_imf: current IMF/ pseudo-IMF
# ndir: number of projections
#
# output:
# env_mean: local envelope for each variate, avg. over all projection directions
# nem: number of extrema 
# amp: absolute difference between upper and lower envelope summed over all 
#      projection directions and variates
#
# details:
# for each proj. direction, the current IMF is projected and the result is summed
# over all variates; for this 2D field, the locations of extrema are determined;
# a grid is fitted through the original IMF values at these locations seperately 
# for each variate, i.e., an upper and lower envelope is determined for each 
# variate; the local mean envelope and amplitude are determined
# 
# =======================================================================================================


def envelope_mean(curr_imf,ndir):
    global M, N, N_dim, dir_vec

    env_mean = np.zeros([M,N,N_dim])
    env_max = np.zeros([M,N,N_dim])
    env_min = np.zeros([M,N,N_dim])
    amp = np.zeros([M,N])
    nem = [''] * ndir
    
    # Projection of input signal on it-th (out of total ndir) direction vectors
    for it in range(ndir):
        # Projection of input signal on direction vector
        y = curr_imf * dir_vec[it]
        # sum over all variates
        y = np.sum(y, axis=2)
        
        # Calculate extrema of the projected signal
        lmax = ndimage.maximum_filter(y,size=3) # local max values
        max_logic = (y == lmax) # convert local max values to binary mask
        lmin = ndimage.minimum_filter(y,size=3)
        min_logic = (y == lmin)
        
        # sum minima and maxima
        # nem stores one value for each projection direction
        nem[it] = np.sum(max_logic) + np.sum(min_logic)
        
        # get indices (rows/columns) of locations of extrema from boolean matrix
        max_loc = np.argwhere(np.transpose(max_logic) == True)
        min_loc = np.argwhere(np.transpose(min_logic) == True)
        
        max_y = max_loc[:,0]
        max_x = max_loc[:,1]
        min_y = min_loc[:,0]
        min_x = min_loc[:,1]
       
        # fit upper and lower envelope to the extrema points; one set of envelopes
        # for each variate
        for i in range(N_dim):
            
            # get values of extrema of current variate
            max_value = curr_imf[max_x,max_y,i]
            min_value = curr_imf[min_x,min_y,i]
            env_max[:,:,i],istop = surffit(max_y,max_x,max_value,list(range(N)),list(range(M)))
            if istop == 0: 
                print('surffit: break due to zero max envelope at ' + str(it) + '. projection direction, ' + str(i) + '. variate', flush=True)
                break
            env_min[:,:,i],istop = surffit(min_y,min_x,min_value,list(range(N)),list(range(M)))
            if istop == 0:
                print('surffit: break due to zero min envelope at ' + str(it) + '. projection direction, ' + str(i) + '. variate', flush=True)
                break
        
        # sum up the amplitude and envelope over all proj. directions  (ampl is summed over all variates,
        # while env_mean is determined separately for each variate)
        amp = amp + np.sqrt(np.sum((env_max - env_min)**2, axis=2))
        env_mean = env_mean + (env_max + env_min)/2

    # division by number of projections to obtain the final envelope for each variate
    env_mean = env_mean/ndir
        
    return env_mean, nem, amp
        
# =======================================================================================================
# stop sifting
#
# input:
# curr_imf: current IMF/ pseudo-IMF
# ndir: number of projections
# depending on stopping criterion two (fix) or three (classic) input arguments
# siftings,nbit (fix), sd, sd2, tol (classic)
#
# output:
# stp: stopping criterion: True to stop sifting
# env_mean: local mean envelope averaged over all projection directions
#
# details:
# function tries to determine local envelope and amplitude and checks if the stopping
# criterion is fullfilled; if determination fails, stop crit is set to True
#
# =======================================================================================================


# function determines local mean envelope and checks for stoppage criterion
def stop_sifting(curr_imf,ndir,*arg):
    global M, N, N_dim
    
    try:
        # determine local envelope and amplitude
        [env_mean, nem, amp] = envelope_mean(curr_imf,ndir)
        
        # average amplitude over all variates
        sx = np.sqrt(np.sum(env_mean**2,axis=2))
        # amp = 0 means no difference between upper and lower envelope
        if np.all(amp==0):
            print("SIFTING warning: amplitude is zero", flush=True)
        else:
            sx = sx/amp
        
        # check stopping criterion
        if len(arg) == 2: # fix siftings
            if arg[0] == (arg[1] + 2): # max number of siftings reached
                stp = 1
            else:
                stp = 0
                    
        elif len(arg) == 3: # traditional stopping criterion
            # define stop criterion value: 1) or 2) needs to be fulfilled to continue sifting
            # 1) avg value of amplitude values greater than sd must be greater than tol
            #   np.mean(sx > sd)*100: percentage, how many entries are true, i.e. how many sx > sd
            #   and tol *100 gives the threshold percentage
            # OR
            # 2a) at least one amplitude value is greater than sd2
            # AND
            # 2b) at least one projection direction has more than 2 extrema
            
            temp1 = np.mean(sx > arg[0]) > arg[2]
            temp2 = np.any(sx > arg[1])
            stp = ~((temp1 | temp2) and np.any(np.array(nem)>2)) # checked
        
    # if envelope & corresponding stop crit cannot be determined, they are set
    # to zero & one (True), i.e., the sifting is terminated    
    except:
        env_mean = np.zeros([M,N,N_dim])
        stp = 1   
        print('SIFTING warning: envelope determination during sifting failed', flush=True)
        
    return stp, env_mean


# =======================================================================================================
# hamm: determine Hammersley sequence (points of n-sphere for direction vectors) 
#
# input:
# ndir: number of projection directions
#
# output:
# seq: low-discrepancy sequences
#
# =======================================================================================================

# Initializations for Hammersley function
def hamm(ndir):
    # init
    global N_dim
    base = np.zeros([N_dim])
    seq = np.zeros([N_dim,ndir])
    base[0] = -ndir
    prm= [2,3,5,7,11,13,17,19,23,29,31,37,41,43,47,53,59,61,67,71,73,79,83,89,97,101,103,107,109,113,127,131,137,139,149]
    
    # Find the pointset for the given input signal
    # trivariate input
    if N_dim==3:
        base[1] = 2
        for it in range (N_dim-1):
            seq[it,:] = hamm_main(ndir,base[it])

    # multivariate input
    elif N_dim>3:
        for iter in range(N_dim-1):
            base[iter+1] = prm[iter]
        
        for it in range(N_dim):
            seq[it,:] = hamm_main(ndir,base[it])

    # no sequence of uni- or bivariate input necessary
    else:
        seq=[]
        
    return seq

# =======================================================================================================
# Hammersley sequence
def hamm_main(ndir,base):
    seq = np.zeros([1,ndir])
    
    if 1 < base:
        seed = list(range(1, ndir+1))
        base_inv = 1/base
        while (np.any(seed != 0)):
            digit = seed % base # give remainder of division
            seq = seq + digit * base_inv
            base_inv = base_inv/base
            seed = np.floor(seed/base)
    else: # first entry of base
        temp = list(range(1, ndir+1))
        seq = (temp % (-base+1) + 0.5)/(-base)

    return seq

    

# =======================================================================================================
# surffit: fit 2D grid (smooth surface approximation) to scattered data 
#
#
# input:
# x,y,z: vectors of equal lengths, containing scattered data
# x,y: coordinates of points with known value
# z: known value corresponding to x,y
#
# xnodes, ynodes: 2D grid on which the fitting should be evaluated; 
#         do need not be equally spaced but must completely span the data.
#
# output:
# zgrid: 2D fitted surface
# istop: binary output if fitting was successful 
#
# =======================================================================================================


def surffit(x,y,z,xnodes,ynodes):

    # prepare input data
    # ===================================================================================================
    
    # check for NaNs in inputs
    nan1,nan2,nan3 = np.argwhere(np.isnan(x)), np.argwhere(np.isnan(y)), np.argwhere(np.isnan(z))
    # if one array contains NaNs, the corresponding row in each array is removed
    if nan1.size != 0 or nan2.size != 0 or nan3.size != 0:
        x,y,z = np.delete(x, nan1), np.delete(y, nan1), np.delete(z, nan1)
        x,y,z = np.delete(x, nan2), np.delete(y, nan2), np.delete(z, nan2)
        x,y,z = np.delete(x, nan3), np.delete(y, nan3), np.delete(z, nan3)
    
    # get step width of grid data and grid size
    dx, dy = np.diff(xnodes), np.diff(ynodes)
    nx, ny = len(xnodes), len(ynodes)
    ngrid = nx*ny
    n = len(x)
    xscale, yscale = np.mean(dx), np.mean(dy)  
 
    
    # generate indices 
    # ===================================================================================================
    indx = np.mod(np.searchsorted(xnodes,x),nx)
    indy = np.mod(np.searchsorted(ynodes,y),ny)
    
    # any point falling at the last node is taken to be inside the last cell in x or y.
    temp = np.argwhere(indx==(nx-1))
    indx[temp] = indx[temp]-1
    temp = np.argwhere(indy==(ny-1))
    indy[temp] = indy[temp]-1
    ind = indy + ny*indx
    
    gridx = np.zeros([x.size])
    gridy = np.zeros([y.size])
    for i in range(len(indx)):
        gridx[i] = min(1,max(0,(x[i] - xnodes[indx[i]])/dx[indx[i]])) 
        gridy[i] = min(1,max(0,(y[i] - ynodes[indy[i]])/dy[indy[i]]))
    
    # linear interpolation 
    temp = (gridx > gridy)
    L = np.ones([n])
    L[temp] = ny
  
    grid_min = np.minimum(gridx,gridy)
    grid_max = np.maximum(gridx,gridy)

    temp_rowIDX = np.concatenate([np.arange(n),np.arange(n),np.arange(n)])
    temp_data = np.concatenate([1-grid_max,grid_min,grid_max-grid_min])
    temp_colIDX = np.concatenate([ind,ind+ny+1,ind+L])   

    mat = csr_matrix((temp_data,(temp_rowIDX,temp_colIDX)),shape=(n,ngrid)) 
    
    
    # prepare fitted data
    # =================================================================================================== 
     # y values
    i,j = np.meshgrid(np.array(range(nx)), np.array(range(1,ny-1)), sparse=False, indexing='xy')
    ind = np.matrix.flatten(np.transpose(j)) + ny*np.matrix.flatten(np.transpose(i))
    dy1 = np.reshape(dy[j-1]/yscale,(ind.size,))
    dy2 = np.reshape(dy[j]/yscale,(ind.size,))
    
    temp_rowIDX = np.concatenate([ind,ind,ind])
    temp_data = np.concatenate([-2/(dy1*(dy1+dy2)),2/(dy1*dy2), -2/(dy2*(dy1+dy2))])            
    temp_colIDX =np.concatenate([ind-1,ind,ind+1])
    mat1 = csr_matrix((temp_data,(temp_rowIDX,temp_colIDX)),shape=(ngrid,ngrid)) 
    
    # x values
    i,j = np.meshgrid(np.array(range(1,nx-1)), np.array(range(ny)), sparse=False, indexing='xy')
    ind = np.matrix.flatten(np.transpose(j)) + ny*np.matrix.flatten(np.transpose(i)) 
    dx1 = np.reshape(dx[i-1]/xscale,(ind.size,))
    dx2 = np.reshape(dx[i]/xscale,(ind.size,))
    
    temp_rowIDX = np.concatenate([ind,ind,ind])
    temp_data = np.concatenate([-2/(dx1*(dx1+dx2)),2/(dx1*dx2), -2/(dx2*(dx1+dx2))])            
    temp_colIDX =np.concatenate([ind-ny,ind,ind+ny])
    mat2 = csr_matrix((temp_data,(temp_rowIDX,temp_colIDX)),shape=(ngrid,ngrid)) 
    Areg = vstack([mat1,mat2])
        
    nreg = ngrid*2 
    nmat = linalg.norm(mat,ord=1) 
    nr = linalg.norm(Areg,ord=1)

    matF = vstack([mat,Areg*(nmat/nr)])
    z = np.concatenate([z,np.zeros(nreg)])
    
    
    # solve with Cholesky decomposition
    # ===================================================================================================
    try:
        factor = cholesky_AAt(matF.T)
        temp = factor(matF.T * z)
        zgrid = np.reshape(temp,(nx,ny))
        zgrid = np.transpose(zgrid)
        
    except:
        #print("surface fitting failed", flush=True)
        # error message is occuring within the "envelope_mean"-function due to zgrid = 0
        zgrid = 0
        
    if np.all(zgrid)==0: istop = 0
    else: istop = 1  
        
    return zgrid, istop
