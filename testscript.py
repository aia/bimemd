# -*- coding: utf-8 -*-
"""
@author: Esther

script to test the 2D NA-MEMD with velocity fluctuation data from a turbulent
boundary layer flow at a friction Reynolds number of 1500
 
"""
# =============================================================================
import h5py
import numpy as np
import os
import fct_biMEMD as biMEMD
# =============================================================================

# init
cwd = os.getcwd()
datapath = cwd + "/data/"
savepath = cwd + "/results/"

filename = "fluctuations_TBL_ReTau1500.hdf5"
savename = "imf_" + filename 

# parameters for NA-MEMD
ndir = 22 # number of projection directions
n_imf = 5  # max number of imfs extracted (includes residual)

# stopping criterion: classical criterion with three parameters (sd,sd2,tol) 
#                     or fixing the number of sifts
# classical:
sd = 0.01
sd2 = 0.1
tol = 0.01
#fixed number of sifts: one number for each IMF
siftings = np.zeros(((n_imf-1),)) 
siftings[0] = 5
siftings[1] = 4
siftings[2] = 4
siftings[3] = 4

# noise parameters
n_nc = 2 # number of noise channels 
sigma = 0.02 # noise power, i.e., variance (sigma * data's variance)

# =============================================================================
# load file and extract velocity components
# =============================================================================

# load file
with h5py.File(datapath + filename, "r") as file:
    # first layer
    list_keys = list(file.keys())[0]
    # second layer
    b = list(file[list_keys].keys())
        
    # =========================================================================
    # extract NW data
    # =========================================================================
    # select data inside box: each entry corresponds to a wall-parallel plane
    # at a different wall-normal distance; first dimension represents spanwise,
    # second dimension streamwise axis
    boxdata = file[list_keys].get(b[0])
    # list with data inside this box
    # 0: nu, 1: nu2d, 2:nu2d_ref, 3: nu_ref, 4: u, 5: u_tau, 6: u_tau2d, 9: v, 10:w
    list_boxdata = list(boxdata.keys())
    
    # extract NW velocity components (positions see above)
    temp = boxdata.get(list_boxdata[4])
    u_fl_NW = np.squeeze(temp[()])
    temp = boxdata.get(list_boxdata[9])
    v_fl_NW = np.squeeze(temp[()])
    temp = boxdata.get(list_boxdata[10])
    w_fl_NW = np.squeeze(temp[()])
    
    
    # =========================================================================
    # extract OL data
    # =========================================================================
    # select data inside box 
    boxdata = file[list_keys].get(b[8])
    # list with data inside this box
    list_boxdata = list(boxdata.keys())
    
    # extract OL velocity components 
    temp = boxdata.get(list_boxdata[4])
    u_fl_OL = np.squeeze(temp[()])
    temp = boxdata.get(list_boxdata[9])
    v_fl_OL = np.squeeze(temp[()])
    temp = boxdata.get(list_boxdata[10])
    w_fl_OL = np.squeeze(temp[()])


# =============================================================================
# reduce streamwise extent
# =============================================================================
xmin, xmax = 792,1314 # arbitrary boundaries
zmin, zmax = 0,375
u_fl_OL_red = u_fl_OL[zmin:zmax,xmin:xmax]
v_fl_OL_red = v_fl_OL[zmin:zmax,xmin:xmax]
u_fl_NW_red = u_fl_NW[zmin:zmax,xmin:xmax]
v_fl_NW_red = v_fl_NW[zmin:zmax,xmin:xmax]

vel_data = np.stack([u_fl_OL_red,v_fl_OL_red,u_fl_NW_red,v_fl_NW_red], axis=1).swapaxes(1,2)

# =============================================================================
# generate Gaussian noise (mu = 0)
# =============================================================================   
 
sz = np.shape(u_fl_OL_red) 
noise = np.zeros((sz[0],sz[1],n_nc)) 
sig = sigma*np.std(vel_data)**2 # variance (power) of noise in relation to variance of velocity data 
for nc in range(n_nc):
    noise[:,:,nc] = np.random.default_rng().normal(0, np.sqrt(sig), (sz[0],sz[1]))
    
# =============================================================================
# combine data, perform biMEMD and save data
# =============================================================================

vel_data_noise = np.concatenate([vel_data,noise], axis=2)
 
imf = biMEMD.getbiMEMD(vel_data_noise,ndir,n_imf,'fix',siftings)

with h5py.File(savepath + savename, "w") as data_file:
    data_file.create_dataset("imfs", data=imf)
    data_file.create_dataset("velNoise", data=vel_data_noise)