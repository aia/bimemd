## Two-dimensional Multivariate Empirical Mode Decomposition

![Representative image](results/chart_MEMD.PNG)

This is the implementation of the Two-dimensional Multivariate Empirical Mode Decomposition (MEMD) based on the single-dimension implementation by Rehman & Mandic (2009) and its bidimensional extension by Xia et al. (2019):

[Rehman & Mandic (2009): 1D Multivariate empirical mode decomposition](https://doi.org/10.1098/rspa.2009.0502)

[Xia et al. (2019): Bidimensional Multivariate Empirical Mode Decomposition With Applications in Multi-Scale Image Fusion](https://ieeexplore.ieee.org/abstract/document/8805082)

### Overview
This repository contains the source code for the 2D MEMD introduced in our paper:

[Analysis of spatiotemporal inner-outer large-scale interactions in turbulent channel flow by multivariate empirical mode decomposition](https://link.aps.org/doi/10.1103/PhysRevFluids.7.034603)

Copyright (C) 2020-2023 Esther Maeteling (C) 2023 Esther Lagemann

This code is published under the GNU General Public License. Please check the "License" document for further information.

## Requirements
The code has been tested with Python 3.6.12 and 3.9.1.
```Shell
conda create --name myMEMD
conda activate myMEMD
conda install -c conda-forge scikit-sparse scipy h5py 
```

## Data
Among others, the code has been used with high-resolution LES data of a turbulent boundary layer flow at a friction Reynolds number of 1500. Details about the computational setup can be found in the publication by
[Albers et al. (2020): Drag reduction and energy saving by spanwise traveling transversal surface waves for flat plate flow](https://link.springer.com/article/10.1007/s10494-020-00110-8).

Exemplary data for a single time step is provided in this repository for testing purposes.


## Evaluation
A short script is provided to test the 2D MEMD code on the provided data. It includes the addition of Gaussian noise to the data prior to decomposition, which is referred to as noise-assisted MEMD (NA-MEMD) and used in our paper to reduce mode mixing. You can also choose between the standard stopping criterion and a fixed number of sifting operations for each IMF. Details on how to use these approaches are given in the MEMD function script. The results of the velocity data decomposition are stored for validation.




